package fr.paragoumba.huffman;

import java.util.AbstractMap.SimpleEntry;
import java.util.*;
import java.util.Map.Entry;

public class Huffman {

    public static byte[] compress(String message){

        char[] messageChars = message.toCharArray();
        HashMap<String, Node> nodesMap = new HashMap<>();

        // Occurrences calculation
        for (char c : messageChars){

            Node node = nodesMap.get(String.valueOf(c));

            // If we never encountered that letter, we add it to the map
            if (node == null) nodesMap.put(String.valueOf(c), new Node(c));
            // Else, we increment its occurrences number
            else node.addOccurrence();

        }

        Node root = null;
        ArrayList<Entry<String, Node>> entrySet = new ArrayList<>(nodesMap.entrySet());

        // Create the tree
        while (entrySet.size() > 1) {

            int minOcc = message.length();
            int secondMinOcc = message.length();
            Node minNode = null;
            Node secondMinNode = null;

            Iterator<Entry<String, Node>> iterator = entrySet.iterator();

            while (iterator.hasNext()){

                Entry<String, Node> entry = iterator.next();

                int occ = entry.getValue().getOccurrences();

                if (occ < minOcc) {

                    minOcc = occ;
                    minNode = entry.getValue();

                } else if (occ < secondMinOcc){

                    secondMinOcc = occ;
                    secondMinNode = entry.getValue();

                }
            }

            root = new Node(Objects.requireNonNull(minNode), Objects.requireNonNull(secondMinNode));
            SimpleEntry<String, Node> rootEntry = new SimpleEntry<>(root.getStr(), root);
            entrySet.add(rootEntry);

            SimpleEntry<String, Node> minNodeEntry = new SimpleEntry<>(minNode.getStr(), minNode);
            entrySet.remove(minNodeEntry);
            System.out.println("Removed " + minNode.getStr());

            SimpleEntry<String, Node> secondMinNodeEntry = new SimpleEntry<>(secondMinNode.getStr(), secondMinNode);
            entrySet.remove(secondMinNodeEntry);
            System.out.println("Removed " + secondMinNode.getStr());

            System.out.println(entrySet);

        }

        Objects.requireNonNull(root);

        // Create the byte sequence
        ArrayList<Node> nodes = new ArrayList<>(nodesMap.values());

        //getCharList(nodes, Objects.requireNonNull(root));
        nodes.sort(Comparator.comparingInt(Node::getOccurrences));

        int lettersNumber = nodes.size();

        byte[] bytes = new byte[lettersNumber * 2 + 2];
        int i = 0;
        bytes[i++] = (byte) (lettersNumber * 2);

        for (; i <= messageChars.length; ++i){

            //System.out.println(messageChars[i]);
            bytes[i] = Byte.parseByte(getBitSequence(messageChars[i], "", root), 2);

        }

        byte currentByte = 0b0;
        int digitsNumber = 0;

        for (char messageChar : messageChars) {

            String digits = getBitSequence(messageChar, "", root);

            for (char c : digits.toCharArray()) {

                currentByte = (byte) (currentByte << 1 | c);
                ++digitsNumber;

                if (digitsNumber == 8) {

                    bytes[i] = currentByte;
                    currentByte = 0b0;
                    ++i;
                    digitsNumber = 0;

                }
            }
        }

        return bytes;

    }

    public static String decompress(byte[] bytes){

        return null;

    }

    private void getCharList(ArrayList<Node> chars, Node node){

        if (node.getStr().length() > 1){

            getCharList(chars, node.getNodeZero());
            getCharList(chars, node.getNodeOne());

        } else chars.add(node);

    }

    private static String getBitSequence(char letter, String bitSequence, Node node) {

        System.out.println(node);
        System.out.println((node.getNodeZero() != null)  + " " + (node.getNodeOne() != null));

        if (node.getNodeZero() != null && node.getNodeOne() != null) {

            System.out.println("Zero : " + node.getNodeZero());
            System.out.println("One : " + node.getNodeOne());

            if (node.getNodeZero().getStr().contains(String.valueOf(letter))) {

                bitSequence = getBitSequence(letter, bitSequence + "0", node.getNodeZero());

            } else if (node.getNodeOne().getStr().contains(String.valueOf(letter))) {

                bitSequence = getBitSequence(letter, bitSequence + "1", node.getNodeOne());

            }
        }

        return bitSequence;

    }
}
