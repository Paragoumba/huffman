package fr.paragoumba.huffman;

public class Node {

    public Node(char c){

        System.out.println("New node : '" + c + "' (" + (byte) c + ')');

        this.str = String.valueOf(c);
        occurrences = 1;

    }

    public Node(Node nodeZero, Node nodeOne){

        System.out.println("New node : " + nodeZero + " + " + nodeOne);

        str = nodeZero.str + nodeOne.str;
        occurrences = nodeZero.occurrences + nodeOne.occurrences;
        this.nodeZero = nodeZero;
        this.nodeOne = nodeOne;

    }

    private String str;
    private int occurrences;
    private Node nodeZero;
    private Node nodeOne;

    public String getStr() {

        return str;

    }

    public int getOccurrences() {

        return occurrences;

    }

    public void addOccurrence(){

        occurrences++;

    }

    public void setOccurrences(int occurrences) {
        this.occurrences = occurrences;
    }

    public Node getNodeZero() {

        return nodeZero;

    }

    public Node getNodeOne() {

        return nodeOne;

    }

    @Override
    public String toString() {

        return "Str : \"" + str + "\",Occ : " + occurrences;

    }
}
