package fr.paragoumba.huffman;

import java.io.IOException;
import java.io.OutputStream;

public class HuffmanOutputStream extends OutputStream {

    public HuffmanOutputStream(OutputStream out){

        this.out = out;

    }

    private OutputStream out;

    @Override
    public void write(int i) throws IOException {

        out.write(i);

    }

    @Override
    public void close() throws IOException {

        super.close();

        if (out != null) out.close();

    }
}
