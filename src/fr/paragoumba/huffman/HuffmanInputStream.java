package fr.paragoumba.huffman;

import java.io.IOException;
import java.io.InputStream;

public class HuffmanInputStream extends InputStream {

    public HuffmanInputStream(InputStream in){

        this.in = in;

    }

    private InputStream in;

    @Override
    public int read() throws IOException {

        return in.read();

    }

    @Override
    public void close() throws IOException {

        super.close();

        if (in != null) in.close();

    }
}
